package com.netshopping.web.controller;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.netshopping.web.model.Brand;

@Controller
public class BrandManagementController {

	private static final Logger logger = LoggerFactory.getLogger(BrandManagementController.class);
	
	public List<Brand> exampleData() {
		List<Brand> brands = new ArrayList<Brand>();
		// 1
		Brand item = new Brand();
		item.setId("914c6288-72f5-4124-9c58-0b5b1e3638ed");
		item.setNo(1);
		item.setCode("Code-1");
		item.setName("Name-1");
		item.setDescription("Description-1");
		item.setUrl("Url-1");
		item.setStatus(false);
		brands.add(item);
		
		// 2
		item = new Brand();
		item.setId("22ad8ba5-ffd1-45e9-b7d8-03ac728f4936");
		item.setNo(2);
		item.setCode("Code-2");
		item.setName("Name-2");
		item.setDescription("Description-2");
		item.setUrl("Url-2");
		item.setStatus(true);
		brands.add(item);
		
		// 3
		item = new Brand();
		item.setId("1ea49501-3e61-43e0-b72d-da56771337bf");
		item.setNo(3);
		item.setCode("Code-3");
		item.setName("Name-3");
		item.setDescription("Description-3");
		item.setUrl("Url-3");
		item.setStatus(false);
		brands.add(item);
		
		return brands;
	}
	
	@RequestMapping(value = "/brand-management", method = RequestMethod.GET)
	public String index() {
		return "brand-management";
	}
	
	@RequestMapping(value = "/api/brand-management", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Object getList() {
		
		//get all data
		logger.info("getList : /api/brand-management : RequestMethod.GET");
		return new ResponseEntity<Object>(exampleData(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/brand-management/{Id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Object getOne(@PathVariable("Id") String Id) {
		
		//get data by Id
		logger.info("getOne : /api/brand-management/"+ Id +" : RequestMethod.GET");
		return new ResponseEntity<Object>(new ArrayList<Object>(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/brand-management", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Object create(@RequestBody Brand newBrand) {
		
		//insert data into database
		logger.info("create : /api/brand-management : RequestMethod.POST");
		return new ResponseEntity<Object>(new ArrayList<Object>(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/brand-management/{Id}", method = RequestMethod.PUT, produces = "application/json")
	public @ResponseBody Object update(@PathVariable("Id") String Id, @RequestBody Brand newBrand) {
		//update data by Id
		logger.info("update : /api/brand-management/"+ Id +" : RequestMethod.PUT");

		return new ResponseEntity<Object>(new ArrayList<Object>(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/brand-management/{Id}", method = RequestMethod.DELETE, produces = "application/json")
	public @ResponseBody Object delete(@PathVariable("Id") String Id) {
		
		//delete data by Id
		logger.info("delete : /api/brand-management/"+ Id +" : RequestMethod.DELETE");
		return new ResponseEntity<Object>(new ArrayList<Object>(), HttpStatus.OK);
	}
}
