<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:importAttribute name="javascripts"/>
<tiles:importAttribute name="stylesheets"/>

<!DOCTYPE html>
<html ng-app="app" ng-controller="AppController">
<head>
    <title><tiles:getAsString name="title" /></title>
    
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta charset="UTF-8">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/google-font/google-font.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/pace-master/themes/blue/pace-theme-flash.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/fontawesome/css/font-awesome.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/bootstrap/css/bootstrap.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/datatables/css/jquery.datatables.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/datatables/css/jquery.datatables_themeroller.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/datatables/css/extension/fixedColumns.dataTables.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/line-icons/simple-line-icons.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/waves/waves.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/switchery/switchery.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/3d-bold-navigation/css/style.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/slidepushmenus/css/component.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/select2/css/select2.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/bootstrap-datepicker/css/datepicker3.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/plugins/sweetalert/sweetalert.css"/>">
    
    <!-- Theme Styles -->
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/main.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/libs.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/css/custom.css"/>">
    
    <script src="<c:url value="/resources/assets/plugins/3d-bold-navigation/js/modernizr.js"/>"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    <c:forEach var="css" items="${stylesheets}">
        <link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
    </c:forEach>
</head>
<body class="page-header-fixed page-horizontal-bar compact-menu ng-cloak">
    <div class="overlay"></div>
    <div ng-controller="<tiles:getAsString name="ngcontroller" />">
        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner container">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="/#/" class="logo-text"><span>NETSHOPPING</span></a>
                    </div>
                    <div class="topmenu-outer">
                        <div class="top-menu">
                            <ul class="nav navbar-nav navbar-left">
                                <li>		
                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
                                </li>
                                <li class="dropdown hide">
                                    <a href="javascript:void(0);" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                        <i class="fa fa-cogs"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-md dropdown-list theme-settings" role="menu">
                                        <li class="li-group">
                                            <ul class="list-unstyled">
                                                <li class="no-link" role="presentation">
                                                    Fixed Header 
                                                    <div class="ios-switch pull-right switch-md">
                                                        <input type="checkbox" class="js-switch pull-right fixed-header-check" checked>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="li-group">
                                            <ul class="list-unstyled">
                                                <li class="no-link" role="presentation">
                                                    Fixed Sidebar 
                                                    <div class="ios-switch pull-right switch-md">
                                                        <input type="checkbox" class="js-switch pull-right fixed-sidebar-check">
                                                    </div>
                                                </li>
                                                <li class="no-link" role="presentation">
                                                    Toggle Sidebar 
                                                    <div class="ios-switch pull-right switch-md">
                                                        <input type="checkbox" class="js-switch pull-right toggle-sidebar-check">
                                                    </div>
                                                </li>
                                                <li class="no-link" role="presentation">
                                                    Compact Menu 
                                                    <div class="ios-switch pull-right switch-md">
                                                        <input type="checkbox" class="js-switch pull-right compact-menu-check" checked>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="no-link"><button class="btn btn-default reset-options">Reset Options</button></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-sidebar sidebar horizontal-bar">
                <div class="page-sidebar-inner">
                    <ul class="menu accordion-menu" ng-menu="">
                        <li class="nav-heading"><span>Navigation</span></li>
                        <li class="droplink"><a href="javascript:void(0);"><span class="menu-icon fa fa-cube"></span><p>Item</p></a>
                            <ul class="sub-menu">
                                <li class="droplink"><a href="#"><p><i class="icon-layers"></i>&nbsp;&nbsp;Code</p><span class="arrow"></span></a>
                                    <ul class="sub-menu">
                                        <li><a href="<c:url value="/brand-management/"/>">Brand Management</a></li>
                                        <li><a href="/#/item-description-code/">Item Description Code</a></li>
                                        <li><a href="/#/item-group-code/">Item Group Code</a></li>
                                        <li><a href="/#/md-management/">MD Management</a></li>
                                        <li><a href="/#/unit-attribute-code/">Unit Attribute Code</a></li>
                                    </ul>
                                </li>
                                <li class="droplink"><a href="#"><p><i class="icon-layers"></i>&nbsp;&nbsp;Item</p><span class="arrow"></span></a>
                                    <ul class="sub-menu">
                                        <li><a href="/#/item-management/">Item Management</a></li>
                                        <li><a href="/#/item-approval/">Item Approval</a></li>
                                        <li><a href="/#/item-description/">Item Description</a></li>
                                        <li><a href="/#/sale-suspend/">Sale Suspend</a></li>
                                        <li><a href="/#/item-group-code-item/">Item Group Code</a></li>
                                    </ul>
                                </li>
                                <li class="droplink"><a href="#"><p><i class="icon-layers"></i>&nbsp;&nbsp;Item Inquiry</p><span class="arrow"></span></a>
                                    <ul class="sub-menu">
                                        <li><a href="/#/item-list/">Item List</a></li>
                                    </ul>
                                </li>
                                <li class="droplink"><a href="#"><p><i class="icon-layers"></i>&nbsp;&nbsp;Vendor</p><span class="arrow"></span></a>
                                    <ul class="sub-menu">
                                        <li><a href="/#/vendor-list/">Vendor List</a></li>
                                        <li><a href="/#/vendor-management/">Vendor Management</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="droplink"><a href="javascript:void(0);"><span class="menu-icon fa fa-bookmark"></span><p>Promotion</p></a>
                            <ul class="sub-menu">
                                <li class="droplink"><a href="#"><p><i class="icon-layers"></i>&nbsp;&nbsp;Promotion</p><span class="arrow"></span></a>
                                    <ul class="sub-menu">
                                        <li><a href="/#/gift-promotion/">Gift Promotion</a></li>
                                    </ul>
                                </li>
                                <li class="droplink"><a href="#"><p><i class="icon-layers"></i>&nbsp;&nbsp;Coupon</p><span class="arrow"></span></a>
                                    <ul class="sub-menu">
                                        <li><a href="/#/coupon-report/">Coupon Report</a></li>
                                        <li><a href="/#/coupon-management/">Coupon Management</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0);"><span class="menu-icon fa fa-rss"></span><p>Broadcast</p></a></li>
                        <li><a href="javascript:void(0);"><span class="menu-icon fa fa-shopping-cart"></span><p>Order</p></a></li>
                        <li><a href="javascript:void(0);"><span class="menu-icon fa fa-money"></span><p>Payment</p></a></li>
                        <li><a href="javascript:void(0);"><span class="menu-icon fa fa-fire"></span><p>Customer</p></a></li>
                        <li><a href="javascript:void(0);"><span class="menu-icon fa fa-truck"></span><p>Delivery</p></a></li>
                        <li><a href="javascript:void(0);"><span class="menu-icon fa fa-paper-plane"></span><p>Consign Logistics</p></a></li>
                        <li><a href="javascript:void(0);"><span class="menu-icon fa fa-bar-chart"></span><p>Analysis</p></a></li>
                        <li><a href="javascript:void(0);"><span class="menu-icon fa fa-cogs"></span><p>System</p></a></li>
                    </ul>
                </div>
            </div>
            <div class="page-inner">
                <!--  <div id="pageContent" ng-view="" class="animated fadeIn"></div>  -->
                <div id="pageContent" class="animated fadeIn">
                    <tiles:insertAttribute name="body" />
                </div>
                <div class="page-footer ng-cloak">
                    <div class="container">
                        <p class="no-s">2015 &copy; NETSHOPPING</p>
                    </div>
                </div>
            </div>
        </main>
    </div>


    <!-- Javascripts -->
	<script src="<c:url value="/resources/assets/plugins/jquery/jquery-2.1.4.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/plugins/jquery-ui/jquery-ui.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/plugins/angularjs/angular-1.4.7/angular.js"/>"></script>
	<script src="<c:url value="/resources/assets/plugins/angularjs/angular-1.4.7/angular-route.js"/>"></script>
	<script src="<c:url value="/resources/assets/plugins/pace-master/pace.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/plugins/jquery-blockui/jquery.blockui.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/bootstrap/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/datatables/js/extension/jquery.jeditable.modify.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/datatables/js/jquery.datatables.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/datatables/js/extension/dataTables.fixedColumns.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/switchery/switchery.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/classie/classie.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/waves/waves.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/3d-bold-navigation/js/main.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/select2/js/select2.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/jquery-browser/jquery.browser.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/moment/moment.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/jquery-dateFormat/jquery-dateFormat.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/chancejs/chance.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/sweetalert/sweetalert.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/plugins/twbs-pagination/jquery.twbsPagination.js"/>"></script>
    
    <!-- Main JS-->
    <script src="<c:url value="/resources/assets/js/main.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/custom.js"/>"></script>
	
	<script src="<c:url value="/resources/assets/js/app.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/directive.js"/>"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			console.log('Master Ready!');
		});
	</script>
	
	<c:forEach var="script" items="${javascripts}">
        <script src="<c:url value="${script}"/>"></script>
    </c:forEach>
</body>
</html>