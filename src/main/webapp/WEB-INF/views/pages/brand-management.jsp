<div class="page-title">
    <div class="container">
        <div class="col-xs-5 with-breadcrumb phn">
            <h3>Brand Management</h3>
        </div>
        <div class="col-xs-7 text-right phn">
            <div class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i>&nbsp;&nbsp;<a href="/#/">Home</a></li>
                    <li><i class="fa fa-cube"></i>&nbsp;&nbsp;<a href="javascript:void(0);">Item</a></li>
                    <li><i class="icon-layers"></i>&nbsp;&nbsp;<a href="javascript:void(0);">Code</a></li>
                    <li class="active">Brand Management</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div id="main-wrapper" class="container">
    <div class="row pbm">
        <div class="col-xs-4 with-button-sm">
            <button class="btn btn-default btn-sm"><i class="fa fa-bookmark-o"></i>&nbsp;User Environment</button>
        </div>
        <div class="col-xs-8 text-right">
            <button class="btn btn-default btn-sm" ng-click="baseButtonClick('Retrieve')"><i class="fa fa-search"></i>&nbsp;&nbsp;Ret.</button>
            <button class="btn btn-default btn-sm" ng-click="baseButtonClick('Insert')"><i class="fa fa-download"></i>&nbsp;&nbsp;Ins.</button>
            <button class="btn btn-default btn-sm" ng-click="baseButtonClick('Delete')"><i class="fa fa-minus-circle"></i>&nbsp;&nbsp;Del.</button>
            <button class="btn btn-default btn-sm" ng-click="baseButtonClick('Save')"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</button>
            <button class="btn btn-default btn-sm" ng-click="baseButtonClick('Print')"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</button>
            <button class="btn btn-default btn-sm" ng-click="baseButtonClick('Excel')"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Xls</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white mbs">
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group form-group-default mbn">
                            <div class="col-xs-7">
                                <label class="control-label pull-left">Brand Code/Name</label>
                                <div class="col-xs-3">
                                    <input type="text" class="form-control">
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-xs-5 text-right prn">
                                <button class="btn btn-default"><i class="fa fa-print"></i>&nbsp;&nbsp;Print All Details</button>
                                <button class="btn btn-default"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Excels</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <table id="table_brand" class="table table-bordered display">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 10% !important">NO.</th>
                                        <th class="text-center" style="width: 20% !important">Brand Code</th>
                                        <th class="text-center">Brand Name</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <div class="table-empty" ng-if="brands.length == 0"><p>No data available in table</p></div>
                        </div>
                        <div class="col-xs-6">
                            <form class="form-horizontal">
                                <div class="form-group form-group-default">
                                    <label class="col-sm-3 control-label">Brand Code/Name</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" ng-model="selectedBrand.code" disabled>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" ng-model="selectedBrand.name">
                                    </div>
                                </div>
                                <div class="form-group form-group-default">
                                    <label class="col-sm-3 control-label">Brand Descirption</label>
                                    <div class="col-sm-9">
                                        <textarea rows="5" class="form-control" ng-model="selectedBrand.description"></textarea>
                                    </div>
                                </div>
                                <div class="form-group form-group-default">
                                    <label class="col-sm-3 control-label">Brand URL</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" ng-model="selectedBrand.url">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

