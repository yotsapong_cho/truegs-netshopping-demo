"use strict";
app.directive('onLastRepeat', function() {
    return function(scope, element, attrs) {
        if (scope.$last) setTimeout(function() {
            scope.$emit('onRepeatLast', element, attrs);
        }, 1);
    };
});

app.directive('dateRangePicker', function(){
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            id: '@',
            middleText: '@',
            dateFormat: '@',
            startDate: '@'
        },
        template: '<div class="input-daterange input-group" id="{{id}}">' +
                        '<input type="text" class="form-control" name="start" />' +
                        '<span class="input-group-addon">{{middleText}}</span>' +
                        '<input type="text" class="form-control" name="end" />' +
                    '</div>',
        link: function(scope, element, attrs){
            $(function(){
                scope.dateFormat = (scope.dateFormat == null) ? 'dd/mm/yyyy' : scope.dateFormat;
                var dateElem = element.datepicker({
                    format: scope.dateFormat,
                    startDate: scope.startDate,
                });
                
                dateElem.on('changeDate', function(ev, date){

                });

                element.bind('blur keyup change', function(ev) {
                   
                });
            });
        }
    }
});


//<date-range-picker id="datepicker-range" middle-text="~"></date-range-picker>

//<div class="input-daterange input-group">
//    <input type="text" class="form-control" name="start" />
//    <span class="input-group-addon">~</span>
//    <input type="text" class="form-control" name="end" />
//</div>

app.directive('tableCheckbox', function(){
    return {
        restrict: 'E',
        replace: true,
        scope: {
            checkId: '@',
            checkCaption: '@',
            checkTheme: '@',
            checkModel: '=',
            actionChange: '&',
            checked: '=',
            disabled: '=',
        },
        template: '<div class="checkbox check-{{checkTheme}} checkbox-edit {{class}}">' + 
                        '<input type="checkbox" id="{{checkId}}" ng-checked="{{checked}}" ng-disabled="{{disabled}}" ng-model="checkModel" ng-click="changeClick({ event: $event, checked: checkModel})">' +
                        '<label for="{{checkId}}"></label>' +
                    '</div>',
        link: function(scope, element, attrs){
            if(scope.checkCaption != null){
                scope.class = 'has-caption';
                element[0].lastChild.innerHTML = scope.checkCaption;
            }
            scope.checkTheme = (scope.checkTheme == null) ? 'success' : scope.checkTheme;
            scope.changeClick = function(e, checked){
                scope.actionChange(e, checked);
            };
        }
    }
});

app.directive('tablePagination', function(){
    return {
        restrict: 'E',
        replace: true,
        scope: {
            caption: '@',
            pageSizeValue: '=',
            selectPageSize: '=',
            selectPage: '=',
            totalPage: '@',
            visiblePage: '@',
            actionChange: '&',
        },
        controller: function($scope){
            
            $scope.initPager = function(totalPage, visiblePage){
                if($scope.pager != null) {
                    $scope.pager.twbsPagination('destroy');
                }
                $scope.pager = $('#table_pagination').twbsPagination({
                    totalPages: totalPage,
                    visiblePages: visiblePage,
                    onPageClick: function (event, page) {
                        $scope.selectPage = page;
                        $scope.change({ pageSize: $scope.selectPageSize, page: $scope.selectPage });
                    }
                });
            };
            
            $scope.change = function(selectedValue){
                selectedValue.pageSize = (selectedValue.pageSize.toString().toLowerCase() == 'all') ? -1 : selectedValue.pageSize;
                $scope.actionChange(selectedValue);
            };
            
            $scope.$on('setPage', function (e, page) {
                $scope.selectPage = page;
            });
            
            $scope.$on('setPageSize', function (e, pageSize) {
                $scope.selectPageSize = pageSize;
            });
            
            $scope.$on('initPager', function (e, totalPage, visiblePage) {
                $scope.initPager(totalPage, visiblePage);
            });
        },
        template: '<div class="table-pagination">' +
                        '<div class="item-per-page pull-left">' +
                            '<div class="input-group">' +
                                '<span class="caption-page">{{caption}}</span>' +
                                '<select class="form-control" ng-model="selectPageSize" ng-options="size for size in pageSizeValue" ng-change="change({ pageSize: selectPageSize, page: selectPage })"></select>' +
                            '</div>' +
                        '</div>' +
                        '<ul id="table_pagination" class="pagination pagination-sm pull-right"></ul>' +
                    '</div>',
        link: function(scope, element, attrs){
            scope.pageSizeValue = (scope.pageSizeValue == null) ? [10, 20, 30, 40, 50, 'All'] : scope.pageSizeValue;
            scope.selectPageSize = (scope.selectPageSize == null) ? 10 : scope.selectPageSize;
            scope.selectPage = (scope.selectPage == null) ? 1 : scope.selectPage;
        }
    }
});

