app.controller('BrandManagementController', ['$scope', '$rootScope', '$compile', '$routeParams', '$location', 'BaseService',
    function ($scope, $rootScope, $compile, $routeParams, $location, BaseService) {
        
        $scope.dataSource = [
            {id: 1, no: 1, code: 'C-001', name: 'AAA', datetime: '2015-10-01 01:10', status: 0, remark: ''},
            {id: 2, no: 2, code: 'C-002', name: 'BBB', datetime: '2015-10-02 00:00', status: 0, remark: '', rowState: 1},
            {id: 3, no: 3, code: 'C-003', name: 'CCC', datetime: '2015-10-03 03:30', status: 0, remark: ''},
            {id: 4, no: 4, code: 'C-004', name: 'DDD', datetime: '2015-10-04 04:40', status: 1, remark: ''},
            {id: 5, no: 5, code: 'C-005', name: 'EEE', datetime: '2015-10-05 05:50', status: 1, remark: ''},
        ];
        
        $scope.dataSelectCode = {
            1: 'C-001',
            2: 'C-002',
            3: 'C-003',
            4: 'C-004',
            5: 'C-005',
        };
        
        $scope.dataSelectName = {
            1: 'AAA',
            2: 'BBB',
            3: 'CCC',
            4: 'DDD',
            5: 'EEE',
        };
        
        $scope.oTable = null;
        $scope.focusedRow = null;
        
        angular.element(document).ready(function() {
            
            BaseService.initail();
            
            setTimeout(function(){
                
                $scope.renderTable($scope.dataSource);
                
                $scope.$broadcast('initPager', 20, 3);
            
            }, 100);
            
        });
        
        $scope.renderTable = function(dataSource){
            if(dataSource != null){
                $scope.generateColumnId(dataSource, 'rowId');
                $scope.oTable = $('#example').dataTable({
                    "data": $scope.filterRowState(dataSource),
                    "columns": [{
                        "data": 'no',
                        "class": 'no text-center'
                    }, {
                        "data": 'code',
                        "class": 'col-code editable text-center'
                    }, {
                        "data": 'name',
                        "class": 'col-name editable'
                    }, {
                        "data": 'datetime',
                        "class": 'col-datetime editable text-center',
                        "render": function(data, type, row) {
                            if(data != null && data != ''){
                                return moment(data).format('YYYY/MM/DD');
                                //return moment(data).format('YYYY/MM/DD HH:mm:ss');
                            }
                            return '';
                        }
                    }, {
                        "data": 'status',
                        "class": 'col-status editable checkbox text-center',
                        "render": function(data, type, row){
                            var checked = (data == 0) ? true : false;
                            return '<table-checkbox check-id="'+ row.rowId +'" checked="'+ checked +'" action-change="statusChangeClick(event, checked)"></table-checkbox>';
                        }
                    }, {
                        "data": 'remark',
                        "class": 'col-remark editable',
                    }, {
                        "render": function(data, type, row) {
                            return '<button type="button" class="btn btn-default btn-full btn-radius" title="delete" style="width: 100%;" ng-click="deleteRowClick($event)"><i class="fa fa-trash-o"></i></button>';
                        },
                        "class": 'text-center pdx'
                        
                    }],
                    "createdRow": function( row, data, index ) {
                        if(data.rowState == 1){
                            $(row).addClass('new');
                        }
                        if(data.rowState == 2){
                            $(row).addClass('edit');
                        }
                        if(data.rowState == -1){
                            $(row).addClass('delete');
                        }
                        $compile(row)($scope);
                    },
                    "bDestroy": true,
                    "paging": false,
                    "filter": false,
                    "info": false
                });
                
                setTimeout(function(){
                    
                    $scope.oTable.find('tbody tr').click(function(e) {
                        $.each(this.parentElement.childNodes, function(index, elem){
                            $(elem).removeClass('row-focus');
                        });
                        $(this).addClass('row-focus');
                        $scope.focusedRow = this;
                        
                        var aPos = $scope.oTable.fnGetPosition(e.target);
                        if(aPos != null){
                            var aData = $scope.oTable.fnGetData(aPos[0]);
                        }
                    });
                    
                    $scope.activateEditTable($scope.oTable, 'code', {
                        "data": angular.toJson($scope.dataSelectCode),
                        "type": 'select',
                        "callback": function(value, settings) {
                            $(this).html($scope.dataSelectCode[value]);
                        }
                    });

                    $scope.activateEditTable($scope.oTable, 'name', {
                        "data": angular.toJson($scope.dataSelectName),
                        "type": 'select2',
//                        "select2DropdownWidth": '300px',
                        "callback": function(value, settings) {
                            $(this).html($scope.dataSelectName[value]);
                        }
                    });
                    
                     $scope.activateEditTable($scope.oTable, 'datetime', {
                        "type": 'datePicker',
                        "dateTimeFormat": 'yyyy/mm/dd',
                         //"dateTimeFormat": 'yyyy/mm/dd hh:ii:ss',
                         //"pickTime": true
                    });

                    $scope.activateEditTable($scope.oTable, 'remark', {
                        "type": 'textarea',
                        "rows": 5
                    });
                }, 1000);
            }
        };
        
        $scope.filterRowState = function(data, rowState){
            var dataOutput = [];
            rowState = (rowState == null) ? [-1] : rowState; 
            $.each(data, function(index, item){
                if($.inArray(item.rowState, rowState) < 0){
                    dataOutput.push(item);
                }
            });
            return dataOutput;
        };
        
        $scope.generateColumnId = function(data, columnName, length, pool){
            length = (length == null) ? 8 : length;
            pool = (pool == null) ? 'abcdefghijklmnopqrstuvwxyz' : pool;
            $.each(data, function(index, item){
                item.rowId = chance.string({length: length, pool: pool});
            });
        };
        
        $scope.activateEditTable = function(oTable, columnName, options){
            $(oTable.selector + ' tbody td.col-' + columnName).editable( function(newValue, settings) {
                var rowData = $scope.fnGetData(oTable, this);
                if(settings.type == 'select' || settings.type == 'select2'){
                    if(settings.data != null){
                        var arrData = $.parseJSON(settings.data);
                        newValue = arrData[newValue];
                    }
                }
                var oldValue = rowData[columnName];
                if(settings.type == 'datePicker'){
                    if(moment(oldValue).isValid()){
                        oldValue = moment(oldValue).format(settings.dateMomentFormat);
                    }
                }
                if(oldValue != newValue){
                    if(settings.type == 'datePicker'){
                        if(moment(newValue).isValid()){
                            rowData[columnName] = moment(newValue).format('YYYY-MM-DD HH:mm:ss');
                        }
                    }
                    else {
                        rowData[columnName] = newValue;
                    }
                    if(!$(this.parentElement).hasClass('new')){
                        $(this.parentElement).addClass('edit');
                        rowData['rowState'] = 2;
                    }
                }
                return newValue;
            }, options);
        };
        
        $scope.showDataClick = function(){
            console.log($scope.dataSource);
        };
        
        $scope.addRowClick = function(){
            var maxlistNo = -1;
            $scope.dataSource.forEach(function(item) {
                if(item.rowState != -1){
                    var value = parseInt(item.no);
                    maxlistNo = (value > maxlistNo) ? value : maxlistNo;
                }
            });
            
            var newRow = {
                no: maxlistNo + 1,
                code: '',
                name: '',
                remark: '',
                rowState: 1
            };
            $scope.dataSource.push(newRow);
            $scope.renderTable($scope.dataSource);
        };
        
        $scope.statusChangeClick = function(e, checked){
            var columnElem = e.currentTarget.parentElement.parentElement;
            var rowData = $scope.fnGetData($scope.oTable, columnElem);
            if(rowData != null){
                var newValue = (checked) ? 0 : 1;
                $scope.validRowState(columnElem.parentElement, rowData.status, newValue);
                rowData.status = newValue;
            }
        };
        
        $scope.deleteRowClick = function(e){
            var columnElem = e.currentTarget.parentElement;
            var rowData = $scope.fnGetData($scope.oTable, columnElem);
            if(rowData != null){
                swal({
                    title: "Confirm Delete",
                    text: "Are you sure you want to delete this item ?\r\nNO." + rowData.no,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F8BB86",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel",
                    closeOnConfirm: false
                }, function() {

                    if(!$(columnElem.parentElement).hasClass('new')){
                        $(e.currentTarget.parentElement.parentElement).addClass('delete');
                        rowData['rowState'] = -1;
                    }
                    else {
                        $scope.dataSource.removeValue('rowId', rowData['rowId'])
                    }

                    swal("Deleted!", "This item has been deleted.", "success");
                    $scope.renderTable($scope.dataSource);
                });
            }
        };
        
        $scope.delClick = function(){
            if($scope.focusedRow != null){
                var aPos = $scope.oTable.fnGetPosition($scope.focusedRow.firstChild);
                if(aPos != null){
                    var aData = $scope.oTable.fnGetData(aPos[0]);
                    console.log(aData);
                }
            }
        };
        
        $scope.fnGetData = function(oTable, columnElem){
            var aPos, aData;
            aPos = oTable.fnGetPosition(columnElem);
            if(aPos != null){
                aData = oTable.fnGetData(aPos[0]);
            }
            return aData;
        };
        
        $scope.validRowState = function(elem, oldValue, newValue){
            if(oldValue != newValue){
                if(!$(elem).hasClass('new')){
                    $(elem).addClass('edit');
                }
            }
        };
        
        $scope.pageChange = function(pageSize, page){
            console.log('Page Size : ' + pageSize + '  <---->  Page : ' + page);
            
            //$scope.$broadcast('initPager', 100, 5);
        }
    }
]);