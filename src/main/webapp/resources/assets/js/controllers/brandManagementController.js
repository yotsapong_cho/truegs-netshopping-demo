"use strict";
app.controller('BrandManagementController', ['$scope', '$rootScope', '$compile', '$routeParams', '$location', 'BaseService', 'BrandManagementService',
    function ($scope, $rootScope, $compile, $routeParams, $location, BaseService, BrandManagementService) {
        
        $scope.brands = [];
        $scope.selectedBrand = null;
        $scope.$on('onRetrieveClick', function(){
            BrandManagementService.getAll().then(
                function(result){
                    $scope.brands = result;
                    $scope.renderTable($scope.brands);
                },
                function(result){
                    sweetAlert("Oops...", result, "error");
                }
            );
        });
        
        $scope.$on('onDeleteClick', function(){
            if($scope.selectedBrand != null){
                BrandManagementService.deleteBrand($scope.selectedBrand.id).then(
                    function(result){
                        sweetAlert("Success", "Processing is complete.", "success");
                    },
                    function(result){
                        sweetAlert("Oops...", result, "error");
                    }
                );
            }
            else {
                sweetAlert("Oops...", "No data found to delete.", "error");
            }
        });
        
        $scope.$on('onSaveClick', function(){
            if($scope.selectedBrand != null){
                BrandManagementService.updateBrand($scope.selectedBrand).then(
                    function(result){
                        sweetAlert("Success", "Processing is complete.", "success");
                    },
                    function(result){
                        sweetAlert("Oops...", result, "error");
                    }
                );
            }
            else {
                //sweetAlert("Oops...", "No data to save.", "error");
                
                var data = {
                    no: 4,
                    code: 'Code-4',
                    name: 'Name-4',
                    description: 'Description-4',
                    url: 'Url-4',
                }
                
                BrandManagementService.createBrand(data).then(
                    function(result){
                        sweetAlert("Success", "Processing is complete.", "success");
                    },
                    function(result){
                        sweetAlert("Oops...", result, "error");
                    }
                );
            }
        });
        
        
    
        $scope.renderTable = function(dataSource){
            if(dataSource != null){
                $scope.oTable = $('#table_brand').dataTable({
                    "data": dataSource,
                    "columns": [{
                        "data": 'no',
                        "class": 'no text-center'
                    }, {
                        "data": 'code',
                        "class": 'text-center'
                    }, {
                        "data": 'name'
                    }],
                    "createdRow": function( row, data, index ) {
                        $compile(row)($scope);
                    },
                    "bDestroy": true,
                    "paging": false,
                    "filter": false,
                    "info": false
                });
                
                setTimeout(function(){
                    $scope.oTable.find('tbody tr').click(function(e) {
                        $.each(this.parentElement.childNodes, function(index, elem){
                            $(elem).removeClass('row-focus');
                        });
                        $(this).addClass('row-focus');
                        $scope.focusedRow = this;
                        
                        var aPos = $scope.oTable.fnGetPosition(e.target);
                        if(aPos != null){
                            var aData = $scope.oTable.fnGetData(aPos[0]);
                            
                            BrandManagementService.getOne(aData['id']).then(
                                function(result){
                                    $scope.selectedBrand = aData;
                                },
                                function(result){
                                    debugger
                                }
                            );
                        }
                    });
                },200);
            }
        };
    }
]);

app.service("BrandManagementService", ['$http', '$q', 'BaseService',
    function($http, $q, BaseService) {
        return ({
            getAll: getAll,
            getOne: getOne,
            createBrand: createBrand,
            updateBrand: updateBrand,
            deleteBrand: deleteBrand,
        });

        function getAll() {
            var request = $http({
                method: "GET",
                url: apiUrl + "/api/brand-management",
            });
            return (request.then(BaseService.handleSuccess, BaseService.handleError));
        }
        
        function getOne(id) {
            var request = $http({
                method: "GET",
                url: apiUrl + "/api/brand-management/" + id,
            });
            return (request.then(BaseService.handleSuccess, BaseService.handleError));
        }
        
        function createBrand(data) {
            var request = $http({
                method: "POST",
                url: apiUrl + "/api/brand-management",
                headers: { 
                    'Content-Type': 'application/json' 
                },
                data: data
            });
            return (request.then(BaseService.handleSuccess, BaseService.handleError));
        }
        
        function updateBrand(data) {
            var request = $http({
                method: "PUT",
                url: apiUrl + "/api/brand-management/" + data.id,
                data: data
            });
            return (request.then(BaseService.handleSuccess, BaseService.handleError));
        }
        
        function deleteBrand(id) {
            var request = $http({
                method: "DELETE",
                url: apiUrl + "/api/brand-management/" + id,
                data: []
            });
            return (request.then(BaseService.handleSuccess, BaseService.handleError));
        }
    }
]);