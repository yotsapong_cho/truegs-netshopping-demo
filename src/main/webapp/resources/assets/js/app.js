"use strict";

var apiUrl = 'http://localhost:8080/web';

var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider', '$httpProvider',
    function ($routeProvider, $httpProvider) {
        $httpProvider.defaults.headers.post = {
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
        };
    }
]);

app.controller('AppController', ['$scope', '$rootScope', '$routeParams', '$location', 'BaseService',
    function ($scope, $rootScope, $routeParams, $location, BaseService) {
        BaseService.initail();
        
        angular.element(document).ready(function() {
            $('.input-daterange').datepicker();
        });
        
        $rootScope.baseButtonClick = function(buttonType){
            switch(buttonType){
                case 'Retrieve': $rootScope.$broadcast('onRetrieveClick'); break;
                case 'Insert': $rootScope.$broadcast('onInsertClick'); break;
                case 'Delete': $rootScope.$broadcast('onDeleteClick'); break;
                case 'Save': $rootScope.$broadcast('onSaveClick'); break;
                case 'Print': $rootScope.$broadcast('onPrintClick'); break;
                case 'Excel': $rootScope.$broadcast('onExcelClick'); break;
            }
        };
    }
]);

app.service("BaseService", ['$http', '$q',
    function($http, $q) {
        return ({
            initail: initail,
            handleError: handleError,
            handleSuccess: handleSuccess,
        });
        
        function initail(){

            if ($.browser.webkit) {
                $(document).dblclick(function(evt) {
                    if (window.getSelection)
                        window.getSelection().removeAllRanges();
                    else if (document.selection)
                        document.selection.empty();
                });
            }
            
            $('.nav-tabs a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
            
            
            
//            if($('select').length > 0){
//                $('select').select2();
//            }
            
            setTimeout(function(){
                if(!$.browser.webkit ){
                    $('table.dataTable.table-bordered').width('99.7%');
                }
            },500);
            
            
            moment.createFromInputFallback = function(config) {
              // unreliable string magic, or
              config._d = new Date(config._i);
            }; 
        }
        
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                if (response.status == 0) {
                    return ($q.reject("Can't connect web api."));
                }
                return ($q.reject("An unknown error occurred."));
            }
            // Otherwise, use expected error message.
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);

        }
    }
]);
                        